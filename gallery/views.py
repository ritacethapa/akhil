# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView

from .models import Image
from committee.models import *
class ImageList(ListView):
    model = Image


# Create your views here.
def new_image_list(request):
	committee_id = 1
	if request.method == "POST":
		committee_id = request.POST["committee"]
		image_list = Image.objects.all().filter(committee = request.POST["committee"])
	else:
		image_list = Image.objects.all().filter(committee = "1")
	committee_list = Committee.objects.all()

	return render(request, 'gallery/new_image_list.html',{
        'image_list': image_list,
        'committee_list' : committee_list,
        'committee_id' : int(committee_id),

        })
