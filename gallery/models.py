# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from filebrowser.fields import FileBrowseField
from committee.models import Committee
# Create your models here.

class Image(models.Model):
	image = FileBrowseField(format = "image", max_length=200, directory="images/", blank=True, null=True)
	committee = models.ForeignKey(Committee)
	date_created = models.DateTimeField(auto_now_add=True)
	
	def get_med_thumbnail(self):
		return self.image.version_generate("medium").url

	def get_large_thumbnail(self):
		return self.image.version_generate("large").url

	def get_small_thumbnail(self):
		return self.image.version_generate("small").url



