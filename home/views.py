
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView

from committee.models import *
from pdf.models import *

# Create your views here.
def home(request):
	news = Resource.objects.order_by("-date_created")	
	reports = Pdf.objects.order_by("-date_created")
	return render(request, 'home.html',{
        'news': news,
        'reports' : reports,
        })


# Create your views here.
