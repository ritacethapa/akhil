# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from filebrowser.fields import FileBrowseField
from committee.models import Committee
# from filebrowser.settings import ADMIN_THUMBNAIL
# Create your models here.

class Pdf(models.Model):
	pdf = FileBrowseField(format = "document", extensions=[".pdf",".doc",".csv"], max_length=200, directory="pdf/", blank=True, null=True)
	name = models.CharField(max_length = 200, default = "Report",blank = False, null = False)
	committee = models.ForeignKey(Committee)
	date_created = models.DateTimeField(auto_now_add=True)
	
