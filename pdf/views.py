# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView

from .models import Pdf
from committee.models import Committee

def pdf_list(request):
	committee_id = 1
	if request.method == "POST":
		committee_id = request.POST["committee"]
		pdf_list = Pdf.objects.all().filter(committee = request.POST["committee"])
	else:
		pdf_list = Pdf.objects.all().filter(committee = "1")
		
	committee_list = Committee.objects.all()

	return render(request, 'pdf/pdf_list.html',{
        'pdf_list': pdf_list,
        'committee_list' : committee_list,
        'committee_id' : int(committee_id),

        })