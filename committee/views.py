# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Resource,Committee



class ResourceList(ListView):
    model = Resource


class ResourceDetail(DetailView):
    model = Resource

def resource_list(request):
	committee_id = 1
	if request.method == "POST":
		committee_id = request.POST["committee"]
		resource_list = Resource.objects.all().filter(committee = request.POST["committee"])
	else:
		resource_list = Resource.objects.all().filter(committee = "1")
		
	committee_list = Committee.objects.all()

	return render(request, 'committee/new_resource_list.html',{
        'resource_list': resource_list,
        'committee_list' : committee_list,
        'committee_id' : int(committee_id),

        })