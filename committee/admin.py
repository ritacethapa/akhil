# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from .models import Committee, Resource

admin.site.register(Committee, DraggableMPTTAdmin)
admin.site.register(Resource)
