from django.conf.urls import url
from . import views
app_name = "committee"
urlpatterns = [
    url(r'^$', views.resource_list, name='resource_list'),
    url(r'^resource/(?P<pk>\d+)$', views.ResourceDetail.as_view(), name='resource_detail'),
]
