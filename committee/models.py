# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.shortcuts import reverse
from tinymce.models import HTMLField
from mptt.models import MPTTModel, TreeForeignKey


class Committee(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name


class Resource(models.Model):
    title = models.CharField(max_length=2550)
    content = HTMLField()
    committee = models.ForeignKey(Committee, related_name='resources')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('committee:resource_detail', kwargs={'pk': self.pk})
