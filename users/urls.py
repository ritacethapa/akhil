# from django.conf.urls import url

# from django.contrib.auth import views as auth_views

# # from . import views
# from account.views import Login

# from account.views import register
# from django.views.generic import TemplateView

# app_name = "accounts"
# urlpatterns = [

#     # url(r'^login/$', auth_views.login, {'template_name': 'accounts/login.html'}, name='login'),
#     # url(r'^logout/$', auth_views.logout, {'template_name': 'accounts/logout.html'}, name='logout'),
# 	url(r'^register/$', register, name='register'),
# 	url(r'^success/$',TemplateView.as_view(template_name='success.html') , name='success'),
#     # url(r'^register/$', views.register, name='register'),
#     # url(r'^bldg_new$', views.new_bldg, name='bldg_new'),

#     url(r'^login/$', Login.as_view(), name='login'),
#     # url(r'^logout/$', auth_views.logout, name='logout'),


# ]


# users/urls.py
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^signup/$', views.SignUp.as_view(), name='signup'),
    url(r'^login/$', views.Login.as_view(), name='login'),
]

