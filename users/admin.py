# users/admin.py
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm
from .models import CustomUser

class CustomUserAdmin(admin.ModelAdmin):
    # model = CustomUser
	list_display = ('username','first_name','email')
	search_fields = ('first_name', 'last_name','username','email')
	list_filter = ('post','role',)
	fields = ('username','first_name','last_name','email','membership_no','educational_institude','date_of_issue','confirmed','post','role')
admin.site.register(CustomUser, CustomUserAdmin)
