# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.db import models
# from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser

class Posts(models.Model):
	name = models.TextField(max_length = 100)
	def __str__(self):
		return self.name

class Role(models.Model):
	name = models.TextField(max_length = 100)
	def __str__(self):
		return self.name

class CustomUser(AbstractUser):
    # user = models.OneToOneField(User, on_delete=models.CASCADE)
    membership_no = models.IntegerField(blank = True, null = True, unique = True)
    educational_institude = models.CharField(max_length = 300,blank = True)
    date_of_issue = models.DateField(blank=False, null = False, default = datetime.date(1994,6,30))
    confirmed = models.BooleanField(default = False)
    # birth_date = models.DateField(null=True, blank=True)
    post= models.ForeignKey(Posts,default = 1)
    role= models.ForeignKey(Role,default = 1)

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         UserDetails.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()

