# users/forms.py
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ['username','first_name','last_name','password1','password2','email','educational_institude','date_of_issue']

# class CustomLoginForm(UserChangeForm):

#     class Meta:
#         model = CustomUser
#         fields = ['username','password']


class CustomLoginForm(forms.Form):

  username = forms.CharField()
  password = forms.CharField(widget=forms.PasswordInput)

