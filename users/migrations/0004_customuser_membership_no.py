# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-12 10:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20180212_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='membership_no',
            field=models.IntegerField(blank=True, null=True, unique=True),
        ),
    ]
