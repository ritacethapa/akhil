# users/views.py
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import login, authenticate, logout

from .forms import CustomUserCreationForm,CustomLoginForm
from django.views.generic.edit import FormView
from django.shortcuts import render, redirect
from .models import CustomUser
from django.contrib import messages 

class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

class Login(FormView):
	template_name = 'registration/login.html'
	form_class = CustomLoginForm
	success_url = reverse_lazy('home')

	def get(self, request, *args, **kwargs):
		return render(request, 'registration/login.html', context = {
        	"form": CustomLoginForm()
      	})

   
	def form_valid(self,form):
		print("form valid here")
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']

		user = authenticate(username = username, password = password)

		if user is not None:
			if (user.confirmed != 0):
				login(self.request,user)
			else:
				# print("Not Verified")
				messages.error(self.request,"Verification By Admin is Left")
				user = None

		else:
			# print("user DoesNotExist") 
			messages.error(self.request, "Username / Password is Wrong")

		if user:
			return redirect('home:home')
		else:
			print("now here")
			return render(self.request,'registration/login.html',{'form' : CustomLoginForm})