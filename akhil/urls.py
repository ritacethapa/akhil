# djauth/urls.py
from django.contrib import admin
from django.conf.urls import url,include
from django.views.generic.base import TemplateView
from filebrowser.sites import site
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='home:home', permanent=False)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
   	url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^news/', include('committee.urls')),
    url(r'^gallery/',include('gallery.urls')),
    url(r'^pdf/',include('pdf.urls')),
    url(r'^home/',include('home.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^users/', include('django.contrib.auth.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
